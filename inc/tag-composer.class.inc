<?php

class ApesterTagComposer {

    /**
     * composes interaction tag with given id.
     * @param $interactionId
     * @return string
     */
    public function composeInteractionTag($interactionId)
    {
        return '<interaction id="' . $interactionId . '"></interaction>';
    }

    /**
     * composes automation tag with auth token.
     * @return string
     */
    public function composeAutomationTag()
    {
        return '<interaction data-random="' . $this->getAuthToken() . '"></interaction>';
    }

    public function composeEditorSuggestionsTag($authToken)
    {
        return '<iframe id="apester-field-suggestions" src="' . APESTER_EDITOR_BASE_URL . '/#/editor-suggestions/drupal?access_token=' . $authToken . '" width="100%" border="0"></iframe>';
    }

    /**
     * Returns the publisher token from the DB.
     * @return string
     */
    private function getAuthToken() {
        return variable_get(APESTER_AUTH_TOKEN, null);
    }
}